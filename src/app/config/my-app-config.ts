export default {
    // oidc - OpenID Connect
    oidc: {
        clientId: '0oa36mfwjYSqd9zKg5d6',
        issuer: 'https://dev-2245502.okta.com/oauth2/default',
        redirectUri: 'http://localhost:4200/login/callback',
        scopes: ['openid', 'profile', 'email'],
    }
}
